package com.conygre.spring.trades_rest_api.service;

import java.util.Collection;

import com.conygre.spring.trades_rest_api.entities.Stock;
import com.conygre.spring.trades_rest_api.entities.Trade;
import com.conygre.spring.trades_rest_api.entities.User;

public interface TradeService {
    
    Trade getTradeById(String id);
    Collection<Trade> getTrades();
    Collection<Trade> getTradesByTicker(String ticker);
    void addTrade(Trade trade);
    //void addManyTrades(Collection<Trade> trades);
    void deleteTrade(Trade trade);
    void deleteTradeById(String id);
    void updateTrade(Trade trade) throws UnsupportedOperationException;
    void cancelTrade(Trade trade) throws UnsupportedOperationException;
    void deleteAllTrades();

    Stock getStockById(String id);
    Collection<Stock> getStocks();
    Collection<Stock> getStocksByTicker(String ticker);
    void addStock(Stock stock);
    void deleteStock(Stock stock);
    void deleteStockById(String id);
    void updateStock(Trade trade);
    void addMultipleTrades();

    void createUser();
    User getUser();
    void setUserAmount(double newAmount);

    void cleanDBs();
}