package com.conygre.spring.trades_rest_api.entities;

public enum TradeState {
    CREATED("CREATED"),
    PROCESSING("PROCESSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED"),
    CANCELLED("CANCELLED");

    private String state;

    private TradeState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    } 
}