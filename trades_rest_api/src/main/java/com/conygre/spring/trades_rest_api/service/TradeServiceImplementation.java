package com.conygre.spring.trades_rest_api.service;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import com.conygre.spring.trades_rest_api.data.StockRepository;
import com.conygre.spring.trades_rest_api.data.TradeRepository;
import com.conygre.spring.trades_rest_api.data.UserRepository;
import com.conygre.spring.trades_rest_api.entities.Stock;
import com.conygre.spring.trades_rest_api.entities.Trade;
import com.conygre.spring.trades_rest_api.entities.TradeState;
import com.conygre.spring.trades_rest_api.entities.TradeType;
import com.conygre.spring.trades_rest_api.entities.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeServiceImplementation implements TradeService {

    @Autowired
    private TradeRepository tradeRepo;

    @Autowired
    private StockRepository stockRepo;

    @Autowired
    private UserRepository userRepo;

    final double intialCashAmount = 10000;

    @Override
    public Collection<Trade> getTrades() {
        return tradeRepo.findAll();
    }

    @Override
    public Collection<Trade> getTradesByTicker(String ticker) {
        return tradeRepo.findByTicker(ticker);
    };

    @Override
    public void addTrade(Trade trade) throws IllegalArgumentException {
        Trade checkTradeTicker;
        try {
            checkTradeTicker = new Trade(trade.getTicker(), trade.getQuantity(), trade.getRequestedPrice(),
                    trade.getType());
            tradeRepo.insert(checkTradeTicker);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /*
     * @Override public void addManyTrades(Collection<Trade> trades) {
     * repo.insert(trades); }
     */

    @Override
    public void deleteTrade(Trade trade) {
        tradeRepo.delete(trade);
    }

    @Override
    public void deleteTradeById(String id) {
        tradeRepo.deleteById(id);
    }

    @Override
    public void updateTrade(Trade trade) throws UnsupportedOperationException {
        Trade refCheckTrade = getTradeById(trade.getId());
        if (refCheckTrade != null) {
            switch (refCheckTrade.getTradeState()) {
                case CREATED:
                    tradeRepo.save(trade);
                    break;
                default:
                    throw new UnsupportedOperationException("You can only change the state of a CREATED trade.");
            }
        } else {
            throw new UnsupportedOperationException("Trade does not exist.");
        }
    }

    @Override
    public Trade getTradeById(String id) {
        return tradeRepo.findById(id).get();
    }

    @Override
    public void cancelTrade(Trade trade) throws UnsupportedOperationException {
        try {
            trade.setTradeState(TradeState.CANCELLED);
            tradeRepo.save(trade);
        } catch (UnsupportedOperationException uoe) {
            throw new UnsupportedOperationException(uoe);
        }
    }

    @Override
    public void deleteAllTrades() {
        tradeRepo.deleteAll();
    }

    @Override
    public Stock getStockById(String id) {
        // TODO Auto-generated method stub
        return stockRepo.findById(id).get();
    }

    @Override
    public Collection<Stock> getStocks() {
        // TODO Auto-generated method stub
        return stockRepo.findAll();
    }

    @Override
    public Collection<Stock> getStocksByTicker(String ticker) {
        return stockRepo.findByTickerName(ticker);
    };

    @Override
    public void addStock(Stock stock) {
        // TODO Auto-generated method stub
        stockRepo.insert(stock);
    }

    @Override
    public void deleteStock(Stock stock) {
        // TODO Auto-generated method stub
        stockRepo.delete(stock);

    }

    @Override
    public void deleteStockById(String id) {
        // TODO Auto-generated method stub
        stockRepo.deleteById(id);

    }

    private void failTrade(Trade trade){
        if(trade.getType() == TradeType.SELL) {
            trade.setQuantity(trade.getQuantity() * -1);
        }
        trade.setTradeState(TradeState.REJECTED);
        tradeRepo.save(trade);
    }

    private void newStock(Trade trade){
        Stock stock = new Stock(trade.getTicker(), trade.getTicker(), trade.getQuantity(),
                        trade.getRequestedPrice());
        stockRepo.insert(stock);
    }

    private void updateStockOnTrade(Stock stock, Trade trade){
        int totalQuant = trade.getQuantity() + stock.getQuantity();
        if(totalQuant > 0){
            double newPrice = (trade.getQuantity() * trade.getRequestedPrice()
                + stock.getPrice() * stock.getQuantity()) / totalQuant;
            stock.setPrice(newPrice);
            stock.setQuantity(totalQuant);
            stockRepo.save(stock);
        }else {
            stockRepo.delete(stock);
        }
        
    }

    @Override
    public void updateStock(Trade trade) {
        // TODO Auto-generated method stub
        Collection<Stock> updateList = stockRepo.findByTickerName(trade.getTicker());
        User myUser = this.getUser(); 
        if (updateList.isEmpty()) {
            if (trade.getType() == TradeType.SELL) {
                this.failTrade(trade);
            } else {
                if(myUser.canBuy(trade)){
                    myUser.doTrade(trade);
                    userRepo.save(myUser);
                    this.newStock(trade);
                }else{
                    this.failTrade(trade);
                }
            }
        } else {
            Stock stockElem = updateList.iterator().next();
            if (trade.getQuantity() * -1 > stockElem.getQuantity() || !myUser.canBuy(trade)) {
                this.failTrade(trade);
            } else {
                myUser.doTrade(trade);
                userRepo.save(myUser);
                this.updateStockOnTrade(stockElem, trade);;
            }
        }
    }

    @Override
    public void addMultipleTrades() throws IllegalArgumentException {
        try {
            tradeRepo.insert(new Trade("TSLA", 1, 420.69, TradeType.BUY));
            tradeRepo.insert(new Trade("AAPL", 5, 110, TradeType.BUY));
            tradeRepo.insert(new Trade("C", 10, 50, TradeType.BUY));

        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void createUser() {
        userRepo.insert(new User(this.intialCashAmount));
    }

    @Override
    public User getUser() {
        Collection<User> users = userRepo.findAll();
        if(users.isEmpty()){
            createUser();
            users = userRepo.findAll();
        }
        return users.iterator().next();
    }

    @Override
    public void setUserAmount(double newAmount) {
        User myUser = this.getUser();
        myUser.setCashAmount(newAmount);
        userRepo.save(myUser);
    }

    @Override
    public void cleanDBs(){
        stockRepo.deleteAll();
        tradeRepo.deleteAll();
        userRepo.deleteAll();
    }
}