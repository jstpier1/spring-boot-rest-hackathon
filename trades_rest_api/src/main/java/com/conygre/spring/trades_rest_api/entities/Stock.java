package com.conygre.spring.trades_rest_api.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Stock {
    @Id
    private String id;
    private int quantity;
    private String name;
    private String tickerName;
    private double price;

    public Stock(String name, String tickerName, int quantity, double price){
        this.name = name;
        this.tickerName = tickerName;
        this.quantity = quantity;
        this.price = price;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTickerName() {
        return this.tickerName;
    }

    public void setTickerName(String tickerName) {
        this.tickerName = tickerName;
    }

    public Stock() {
        //TODO not sure if this needed for mongodb
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
