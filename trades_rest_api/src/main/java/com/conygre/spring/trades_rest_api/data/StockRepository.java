package com.conygre.spring.trades_rest_api.data;

import java.util.Collection;

import com.conygre.spring.trades_rest_api.entities.Stock;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockRepository extends MongoRepository<Stock, String> {
    
    public Collection<Stock> findByTickerName(String tickerName);

}