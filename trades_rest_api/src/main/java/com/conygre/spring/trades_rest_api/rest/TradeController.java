package com.conygre.spring.trades_rest_api.rest;

import java.util.Collection;
import java.util.LinkedList;

import com.conygre.spring.trades_rest_api.entities.Stock;
import com.conygre.spring.trades_rest_api.entities.Trade;
import com.conygre.spring.trades_rest_api.entities.User;
import com.conygre.spring.trades_rest_api.service.TradeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/trades")
@CrossOrigin
public class TradeController {
    
    @Autowired
    private TradeService service;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Trade> findAll() {
        return service.getTrades();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/id/{id}")
	public Trade getTradeByID(@PathVariable("id") String id) {
        return service.getTradeById(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/stock/{id}")
	public Stock getStockByID(@PathVariable("id") String id) {
        return service.getStockById(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/stock")
	public Collection<Stock> getStock() {
        return service.getStocks();
    }

    @RequestMapping(method = RequestMethod.POST, value="/stock")
	public void addStock(@RequestBody Stock stock) throws IllegalArgumentException {
		try {
            service.addStock(stock);
        } catch (IllegalArgumentException iae) {
            throw new IllegalArgumentException(iae);
        } 
            
    }

    @RequestMapping(method = RequestMethod.POST, value="/stock/buy")
	public void addBuyStock(@RequestBody Trade trade) throws IllegalArgumentException {
		try {
            service.updateStock(trade);
        } catch (IllegalArgumentException iae) {
            throw new IllegalArgumentException(iae);
        } 
            
    }

    @RequestMapping(method = RequestMethod.POST, value="/stock/sell")
	public void addSellStock(@RequestBody Trade trade) throws IllegalArgumentException {
		try {
            trade.setQuantity(trade.getQuantity() * -1);
            service.updateStock(trade);
        } catch (IllegalArgumentException iae) {
            throw new IllegalArgumentException(iae);
        } 
            
    }

    @RequestMapping(method = RequestMethod.GET, value="/myuser")
	public User getUserInfo() throws IllegalArgumentException {
		return service.getUser(); 
    }

    // @RequestMapping(method = RequestMethod.PUT, value="/stock")
	// public void updateAddStock(@RequestBody Stock stock) throws IllegalArgumentException {
	// 	try {
    //         service.updateStock(stock);
    //     } catch (IllegalArgumentException iae) {
    //         throw new IllegalArgumentException(iae);
    //     } 
            
    // }
    
    @RequestMapping(method = RequestMethod.GET, value = "/{ticker}")
	public Collection<Trade> getTradesByTicker(@PathVariable("ticker") String ticker) {
		return service.getTradesByTicker(ticker);
	}

    @RequestMapping(method = RequestMethod.POST)
	public void addTrade(@RequestBody Trade trade) throws IllegalArgumentException {
		try {
            service.addTrade(trade);
        } catch (IllegalArgumentException iae) {
            throw new IllegalArgumentException(iae);
        } 
            
    }

    /*@RequestMapping(method = RequestMethod.PUT)
	public void addManyTrades(@RequestBody Collection<Trade> trade) {
		service.addManyTrades(trade);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteTrade(@RequestBody Trade trade) {
        service.deleteTrade(trade);
    }*/

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteTradeById(@PathVariable("id") String id) {
        service.deleteTradeById(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateTrade(@RequestBody Trade trade) {
        service.updateTrade(trade);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteAllTrades() {
        service.deleteAllTrades();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/test")
	public void addMultipleTrades() throws IllegalArgumentException {
        service.cleanDBs();
        service.addMultipleTrades();
            
    }
    
}