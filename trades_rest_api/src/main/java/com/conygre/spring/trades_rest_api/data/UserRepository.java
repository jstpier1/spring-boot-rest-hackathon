package com.conygre.spring.trades_rest_api.data;

import com.conygre.spring.trades_rest_api.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

}