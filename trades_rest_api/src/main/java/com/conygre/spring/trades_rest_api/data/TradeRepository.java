package com.conygre.spring.trades_rest_api.data;

import java.util.Collection;

import com.conygre.spring.trades_rest_api.entities.Trade;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade, String> {
    
    public Collection<Trade> findByTicker(String ticker);

}