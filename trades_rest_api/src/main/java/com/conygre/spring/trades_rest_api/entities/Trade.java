package com.conygre.spring.trades_rest_api.entities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade implements java.io.Serializable{

    @Id
    private String id;
    private Date dateCreated;
    private String ticker;
    private int quantity;
    private double requestedPrice;
    private TradeState state;
    private TradeType type;


    public Trade(String ticker, int quantity, double requestedPrice, TradeType type) throws IllegalArgumentException, IOException {
        this.dateCreated = new Date();
        this.state = TradeState.CREATED;
        
        this.ticker = ticker;
        // Validate ticker
        // This is ugly, pls don't look!
        URL url = new URL(String.format("https://financialmodelingprep.com/api/v3/search?query=%s&apikey=demo", ticker));
        boolean empty = true;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
            String line = reader.readLine();
            if(line == null) {
                empty = true;
            } else {
                for (String line2; (line2 = reader.readLine()) != null;) {
                    if(!line2.isEmpty()) empty = false;
                }
            }
        }

        if(!empty) {
            this.ticker = ticker;
        } else {
            throw new IllegalArgumentException("Invalid ticker submitted.");
        } 
       
        this.quantity = quantity;
        this.requestedPrice = requestedPrice;
        this.type = type;
    }

    public Trade() {
        this.dateCreated = new Date();
        this.state = TradeState.CREATED;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public TradeState getTradeState() {
        return state;
    }

    public void setTradeState(TradeState state) throws UnsupportedOperationException {
        if(!this.state.equals(TradeState.CREATED)) {
            throw new UnsupportedOperationException("You can only change the state of a CREATED trade.");
        } else {
            this.state = state;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Trade)) return false; 
        Trade otherTrade = (Trade) o; 
        return otherTrade.getId().equals(this.getId());
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }





    
}
