package com.conygre.spring.trades_rest_api.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User {
    @Id
    private String id;
    private double cashAmount;

    public User(double cashAmount){
        this.cashAmount = cashAmount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(double cashAmount) {
        this.cashAmount = cashAmount;
    }

    public boolean canBuy(Trade trade){
        return cashAmount >= (trade.getQuantity() * trade.getRequestedPrice());
    }

    public void doTrade(Trade trade){
        this.cashAmount = this.cashAmount - (trade.getQuantity() * trade.getRequestedPrice());
    }

    
}
