package com.conygre.spring.trades_rest_api.entities;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TradeTest {


    @Test
    public void canCatchTickerError() {
        assertThrows(IllegalArgumentException.class, () -> {
            Trade test = new Trade("!!!!", 22, 220.00,TradeType.BUY);
            System.out.println(test.getTicker());
        });
    }

    @Test
    public void canValidateRealTicker() {
        assertDoesNotThrow(() -> { 
            Trade test2 = new Trade("AAPL", 22, 220.00,TradeType.BUY);
            System.out.println(test2.getTicker());
        });
        
    }
}