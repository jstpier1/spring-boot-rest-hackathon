package com.conygre.spring.trades_rest_api.data;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import com.conygre.spring.trades_rest_api.entities.Trade;
import com.conygre.spring.trades_rest_api.entities.TradeState;
import com.conygre.spring.trades_rest_api.entities.TradeType;
import com.conygre.spring.trades_rest_api.service.TradeServiceImplementation;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TradeRepositoryTest {

    private Trade trade;
    private final String dummyID = "5f4969d236076056a358acd8";
    private String ticker;

    @Autowired
    private TradeRepository repo;

    @Autowired
    private TradeServiceImplementation service;

    @BeforeEach
    public void createAndInsertDummyTrade() throws IllegalArgumentException, IOException {
        trade = new Trade("GOOG", 10, 1634.12, TradeType.BUY);
        trade.setId(dummyID);
        ticker = "GOOG";
    }

    @Test
    public void canRetrieveTradeByTicker() {
        repo.insert(trade);
        Iterable<Trade> trades = repo.findByTicker(ticker);
        boolean result = false;
        for (Trade current : trades) {
            if (current.getTicker().equals(ticker)) {
                result = true;
                break;
            }
        }
        assertTrue(result);
    }

    @Test
    public void compactDiscServiceCanReturnACatalog() {
        repo.insert(trade);
        Iterable<Trade> trades = service.getTrades();
        boolean result = false;
        for (Trade current : trades) {
            if (current.getTicker().equals(ticker)) {
                result = true;
                break;
            }
        }
        assertTrue(result);

    }

    @Test
    public void canAddTrade() {
        int prevSize = (int) repo.count();
        service.addTrade(trade);
        int newSize = (int) repo.count();
        assertTrue(newSize == prevSize + 1);
    }

    @Test
    public void canGetTradeById() {
        service.addTrade(trade);
        Trade result = service.getTradeById(dummyID);
        assertTrue(result != null);
    }

    @Test
    public void canDeleteTrade() {
        service.addTrade(trade);
        int prevSize = (int) repo.count();
        service.deleteTrade(trade);
        int newSize = (int) repo.count();
        assertTrue(newSize == prevSize - 1);
    }

    @Test
    public void canDeleteTradeById() {
        // Put it in
        service.addTrade(trade);
        int prevSize = (int) repo.count();
        
        // Attempt delete
        service.deleteTradeById(dummyID);
        int newSize = (int) repo.count();
        assertTrue(newSize == prevSize - 1);
    }

    @Test
    public void canUpdateTrade() {
        // Put it in
        service.addTrade(trade);

        // Get existing trade data
        Trade result = service.getTradeById(dummyID);
        assertTrue(result != null);
        String oldTicker = result.getTicker();

        // Update ticker field in object
        result.setTicker("AMD");

        // Attempt to update in DB
        service.updateTrade(result);
        
        // Pull object down again by ID
        Trade updatedResult = service.getTradeById(dummyID);
        assertTrue(updatedResult != null);
        String newTicker = updatedResult.getTicker();

        // Compare updated fields
        assertTrue(!oldTicker.equals(newTicker));
    }

    @Test
    public void canUpdateTradeState() {
        // insert into DB; default state is CREATED
        service.addTrade(trade);

        // pull and get old state
        Trade result = service.getTradeById(dummyID);
        assertTrue(result != null);
        TradeState oldState = result.getTradeState();
    
        // attempt to update state
        TradeState proposedNewState = TradeState.FILLED;
        result.setTradeState(proposedNewState);
        service.updateTrade(result);

        // compare old and new state from pulled result
        Trade result2 = service.getTradeById(dummyID);
        assertTrue(result2 != null);
        TradeState actualNewState = result2.getTradeState();

        assertTrue(!oldState.equals(actualNewState));
    }

    @Test
    public void cannotUpdatePermanentTradeState() throws IllegalArgumentException, IOException {
        // set state to something permanent, i.e. not CREATED, and add to DB
        // TradeState usedState = TradeState.PROCESSING;
        // trade.setTradeState(usedState);
        // service.addTrade(trade);

        // // pull and verify state is not CREATED
        // Trade result = service.getTradeById(dummyID);
        // assertTrue(result != null);
        // TradeState pulledState = result.getTradeState();
        // assertTrue(!pulledState.equals(TradeState.CREATED));
    
        // // attempt to update state -- need to create a new identical obj here with CREATED or else UOE thrown prematurely
        // TradeState proposedNewState = TradeState.FILLED;
        // trade = new Trade("GOOG:A", 10, 1634.12);
        // trade.setId(dummyID);
        // trade.setTradeState(proposedNewState);

        // // Listen for exception; pass test is UOE thrown
        // assertThrows(UnsupportedOperationException.class, () -> {
        //     service.updateTrade(result);;
        // });

       
    }

    @Test
    public void canCancelTrade() {
        // Add trade
        TradeState oldState = trade.getTradeState();
        service.addTrade(trade);
        service.cancelTrade(trade);

        // Grab trade
        Trade updatedResult = service.getTradeById(dummyID);
        TradeState newState = updatedResult.getTradeState();
        
        assertTrue(!oldState.equals(newState));

    }

    @AfterEach
    public void deleteDummyTrade() {
        service.deleteTradeById(dummyID);
    }


}