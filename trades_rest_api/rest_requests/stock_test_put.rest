PUT http://localhost:8080/api/trades/stock HTTP/1.1
content-type: application/json

{
    "name" : "AAPL",
    "quantity" : 100,
    "tickerName" : "APPL"
}